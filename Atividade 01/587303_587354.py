 # -*- coding: utf-8 -*-
"""
Elisa Castro    587303
Letícia Berto   587354
"""
import socket
import sys
from threading import Thread
from datetime import datetime
import struct

class Fila(object):
    def __init__(self):
        self.dados = []

    def insere(self, elemento):
        self.dados.append(elemento)

    def retira(self):
        return self.dados.pop(0)

    def ordTotal(self, elemento):
        for list in self.dados:
            list[2] = str(list[2]) + str(elemento)

    def imprime(self):
        for list in self.dados:
            print "\tMensagem: " + str(list[0]) + " pId: " + str(list[1]) + " clock: " + str(list[2])

def enviar():
    mensagem = raw_input("Digite a mensagem que deseja enviar: ")
    # Cria o socket datagrama - UDP
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)


    # Seta o TTL das mensagens para 1, para não sair da rede local
    ttl = struct.pack('b', 1)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)

    enviar_mensagem = mensagem + "@#*" + str(pId) + "@#*" + str(clock)

    try:
        sock.sendto(enviar_mensagem.encode('utf-8'), multicast_group_cliente) # Envia uma mensagem através do socket para aquela porta (udp)
        numAck = 0;
        while True:
            dados, server = sock.recvfrom(tam_buffer) #recebe resposta do servidor, dados = mensagem; server = endereço
            if len(str(dados)) >= 0:
                men_ack, pid_ack = dados.split('@#*')
                # Verificando se o ACK recebido corresponde a mensagem que enviei
                if pid_ack == pId:
                    numAck = numAck + 1
                    if numAck == 3: #quantidade de processos definido em um ambiente controlado
                        print "\nCliente: "
                        print ("Recebi ack da mensagem '" + mensagem + "' de todos os processos\n")
                        break
            else:
                print("Nada foi recebido!")
    except ValueError:
        print("Erro de conexao no cliente")
    finally:
        sock.close()

def receber():

    # Cria o socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    #COLOCA aqui para poder reusar a porta em mais de um servidor - tira o uso exclusivo de apenas um server
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    # ligar com o endereço do servidor
    sock.bind(server_address)

    # Adicionar o socket no grupo multicast de todas as interfaces
    group = socket.inet_aton(multicast_group)
    mreq = struct.pack('4sL', group, socket.INADDR_ANY)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

    # inicializando a fila
    fila = Fila()

    #while TRUE:
    for x in range(0,3): #quantidade de processos em ambiente controlado
        try:
            dados, server = sock.recvfrom(tam_buffer) # Recebe uma mensagem do tamanho BUFFER_SIZE
            if len(str(dados)) >= 0:
                print "\nServidor: "
                men_local, pid_local, clock_local = dados.split('@#*')

                #clock do processo, atualizado (lamport)
                global clock
                clock = max(int(clock_local), int(clock)) + 1 #ordenação parcial

                #print "\nclock da mensagem que recebi: " + str(clock_local) + " seu pid: " + str(pid_local)

                #coloca os elementos em uma lista, em uma fila
                fila.insere([men_local, pid_local, clock])
                print "Fila do processo (ordenada parcialmente): "
                fila.imprime()
                print "Fim da fila\n"

                print ("Mensagem vinda do cliente: " + men_local.decode('UTF-8') + "\n")
                mensagem_servidor = '' #limpa a mensagem para enviar correspondente a cada processo recebido
                mensagem_servidor = "ACK@#*" + str(pid_local)
                sock.sendto(mensagem_servidor.encode('utf-8'), server) #envia resposta para o cliente
            else:
                print("Nada foi recebido!")
        except ValueError:
            print("Erro de conexao no servidor.")
    #ordenação Total
    fila.ordTotal(pId) #concatena o pId ao clock
    print "\nFila do processo (ordenacao total): "
    fila.imprime()

if __name__== "__main__":
    """
    servidor = "225.123.203.102"
    porta = 8675
    host = "225.123.203.102"
    """
    tam_buffer = 1024

    multicast_group_cliente = ('224.3.29.71', 10000)
    server_address = ('', 10000)
    multicast_group = '224.3.29.71'

    #pega a hora do sistema para definir o clock
    now = datetime.now()
    clock = now.second
    pId = raw_input("Digite o pId do processo: ")
    print ("\nProcesso pId: " + str(pId) + ", Clock: " + str(clock))
    try:
        t = Thread(target=receber, args=())
        f = Thread(target=enviar, args=())
        t.start()
        f.start()
    except:
        print("Falha ao abrir as threads.")
