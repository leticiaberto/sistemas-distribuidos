 # -*- coding: utf-8 -*-
import socket
import sys
#import thread
from threading import Thread
from datetime import datetime
import struct

class Fila(object):
    def __init__(self):
        self.dados = []

    def insere(self, elemento):
        self.dados.append(elemento)

    def retira(self):
        return self.dados.pop(0)

    def ordTotal(self, elemento):
        for list in self.dados:
            #print "imprimindo lista: " + str(list)
            #print "\nlist[2]: " + str(list[2]) + " pid: " + str(elemento)
            list[2] = str(list[2]) + str(elemento)

    def imprime(self):
        for list in self.dados:
            print "\tMensagem: " + str(list[0]) + " pId: " + str(list[1]) + " clock: " + str(list[2])
            #for x in list:
            #    print x

def enviar():
    mensagem = raw_input("Digite a mensagem que deseja enviar: ")
    #print ("olar " + mensagem)
    # Create the datagram socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)


    # Set the time-to-live for messages to 1 so they do not go past the
    # local network segment.
    ttl = struct.pack('b', 1)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)

    enviar_mensagem = mensagem + "@#*" + str(pId) + "@#*" + str(clock)
    #print ("mengsaem" + enviar_mensagem)

    try:
        #print("Enviar mensagem " + enviar_mensagem + " para o servidor.")
        sock.sendto(enviar_mensagem.encode('utf-8'), multicast_group_cliente) # Envia uma mensagem através do socket para aquela porta (udp)
        #print ("envia")
        #sock.close()
        numAck = 0;
        while True:
            #print ("antes de receber do server")
            dados, server = sock.recvfrom(tam_buffer)
            #dados = sock.recv(tam_buffer) # Recebe mensagem enviada pelo socket.
            #print ("depois de receber do server")
            if len(str(dados)) >= 0:
                men_ack, pid_ack = dados.split('@#*')
                #print "mensagem do servidor: " + men_ack + "pid: " + str(pid_ack)
                # Verificando se o ACK recebido corresponde a mensagem que enviei
                if pid_ack == pId:
                    #print("\nMensagem recebida do servidor: " + dados.decode('utf-8'))
                    numAck = numAck + 1
                    if numAck == 3:
                        print "\nCliente: "
                        print ("Recebi ack da mensagem '" + mensagem + "' de todos os processos\n")
                            #numAck = 0
                        break
            else:
                print("Nada foi recebido!")
    except ValueError:
        print("Erro de conexao no cliente")
    finally:
        sock.close()

def receber():

    #mensagem_servidor = 'ACK' #colocar identificador do processo
    # Create the socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    #COLOCA aqui para poder reusar a porta em mais de um servidor - tira o uso exclusivo de apenas um server
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    # Bind to the server address
    sock.bind(server_address)

    # Tell the operating system to add the socket to the multicast group
    # on all interfaces.
    group = socket.inet_aton(multicast_group)
    mreq = struct.pack('4sL', group, socket.INADDR_ANY)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

    #print("Servidor espera a conexao udp na porta 8675")

    # inicializando a fila
    fila = Fila()

    #while TRUE:
    for x in range(0,3):
        try:
            #print ("entrei")
            #(conexao, identificador) = socket_.accept() # aceita conexoes e recupera o endereco do cliente.
            #print(identificador[0] + " conectado")
            dados, server = sock.recvfrom(tam_buffer) # Recebe uma mensagem do tamanho BUFFER_SIZE
            #print "teste"
            if len(str(dados)) >= 0:
                print "\nServidor: "
                men_local, pid_local, clock_local = dados.split('@#*')
                #print "\nclock do processo: " + clock + " pid do processo: " + pId

                #clock do processo, atualizado (lamport)
                global clock
                clock = max(int(clock_local), int(clock)) + 1 #ordenação parcial

                #print "\nclock da mensagem que recebi: " + str(clock_local) + " seu pid: " + str(pid_local)

                #coloca os elementos em uma lista, em uma fila
                fila.insere([men_local, pid_local, clock])
                print "Fila do processo (ordenada parcialmente): "
                fila.imprime()
                print "Fim da fila\n"

                print ("Mensagem vinda do cliente: " + men_local.decode('UTF-8') + "\n")
                mensagem_servidor = ''
                mensagem_servidor = "ACK@#*" + str(pid_local)
                # print(identificador[0] + " Mensagem do cliente: " + dados.decode('UTF-8'))
                #print("\nResposta para o cliente: " + mensagem_servidor)
                sock.sendto(mensagem_servidor.encode('utf-8'), server)
                #print "Servidor enviou o ack de resposta."
				#conexao.send(mensagem_servidor.encode('utf-8')) # Envia mensagem através do socket.
            else:
                print("Nada foi recebido!")
        except ValueError:
            print("Erro de conexao no servidor.")
    #ordenação Total
    fila.ordTotal(pId)
    print "\nFila do processo (ordenacao total): "
    fila.imprime()

if __name__== "__main__":
    """
    servidor = "225.123.203.102"
    porta = 8675
    host = "225.123.203.102"
    """
    tam_buffer = 1024

    multicast_group_cliente = ('224.3.29.71', 10000)
    server_address = ('', 10000)
    multicast_group = '224.3.29.71'

    #pega a hora do sistema
    now = datetime.now()
    clock = now.second
    pId = raw_input("Digite o pId do processo: ")
    print ("\nProcesso pId: " + str(pId) + ", Clock: " + str(clock))
    try:
        #thread.start_new_thread(receber,())
        #thread.start_new_thread(enviar,())
        t = Thread(target=receber, args=())
        f = Thread(target=enviar, args=())
        t.start()
        f.start()
    except:
        print("Falha ao abrir as threads.")
