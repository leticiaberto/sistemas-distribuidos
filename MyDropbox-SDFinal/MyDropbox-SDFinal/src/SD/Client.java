package SD;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import static java.nio.charset.CoderResult.OVERFLOW;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.Scanner;
import java.util.Collections;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.transfer.Download;

import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;
public class Client {

    private static final String bucketName = "mydropboxsd";
    private static final String projectPath = "/home/leticia/Desktop/MyDropbox-SDFinal/MyDropbox-SDFinal/";
    private static boolean downloadedFromCloud = false;
    
    private static SD.Download d = new SD.Download(bucketName, projectPath);
    private static SD.Upload u = new SD.Upload(bucketName, projectPath);

    private static void synchronize(AmazonS3 s3client) throws IOException {
        /*  Synchronizing with the files in the cloud  */
        ArrayList<String> S3Files = getS3Filenames(s3client); //get the files stored in S3
        ArrayList<String> localFiles = getLocalFilenames(); //get the files stored in local directory
        
        ArrayList<String> toDownload = d.getFilenamesToDownload(S3Files, localFiles); //files in the cloud that doesnt exist locally
        ArrayList<String> toUpload = u.getFilenamesToUpload(S3Files, localFiles); //files that exists locally but not in the cloud
        d.downloadAllFiles(s3client, toDownload);
        u.uploadAllFiles(s3client, toUpload);
    }

    private static ArrayList<String> getS3Filenames(AmazonS3 s3client) {
       ArrayList<String> files = new ArrayList<String>();

       ListObjectsRequest lor = new ListObjectsRequest()
                            .withBucketName(bucketName);
       ObjectListing objectListing = s3client.listObjects(lor);
       for (S3ObjectSummary summary: objectListing.getObjectSummaries()) {
           files.add(summary.getKey());
       }

       Collections.sort(files);
       return files;
    }

    private static ArrayList<String> getLocalFilenames() {
        File folder = new File(projectPath + "Arquivos");
        File[] listOfFiles = folder.listFiles();

        ArrayList<String> localFiles = new ArrayList<String>();
        for (int i = 0; i < listOfFiles.length; i++) {
            localFiles.add(listOfFiles[i].getName());
        }

        Collections.sort(localFiles);
        return localFiles;
    }

    private static void WatchDir(AmazonS3 s3client) throws IOException, InterruptedException {

        /* Cloud changes */
        detectS3Changes(s3client);

        /* Local changes */
        WatchService watcher = FileSystems.getDefault().newWatchService();
        Path dir = Paths.get(projectPath + "Arquivos");
        dir.register(watcher, ENTRY_CREATE, ENTRY_DELETE);

        while (true) {
            WatchKey key;
            try {
                // wait for a key to be available
                key = watcher.take();
            } catch (InterruptedException ex) {
                return;
            }

            for (WatchEvent<?> event : key.pollEvents()) {
                // get event type
                WatchEvent.Kind<?> kind = event.kind();

                // get file name
                @SuppressWarnings("unchecked")
                WatchEvent<Path> ev = (WatchEvent<Path>) event;
                Path fileName = ev.context();

                System.out.println("Modificação -> " + kind.name() + ": " + fileName);


                if (kind == ENTRY_CREATE) {
                    Thread.sleep(500);
                    Upload.uploadObjectSingleOperation(s3client, fileName.getFileName().toString());
                    downloadedFromCloud = false;
                    Thread.sleep(500);
                } else if (kind == ENTRY_DELETE) {
                    Thread.sleep(500);
                        s3client.deleteObject(new DeleteObjectRequest(bucketName, fileName.getFileName().toString()));
                        System.out.println("O arquivo '" + fileName.getFileName().toString() + "' foi removido do S3.");
                }
            }

            // IMPORTANT: The key must be reset after processed
            boolean valid = key.reset();
            if (!valid) {
                break;
            }

            Thread.sleep(1000);
        }


    }


    /* A cada 2 segundos verifica se existem arquivos na cloud que nao estavam la anteriormente */
    private static void detectS3Changes(final AmazonS3 s3client) {
        (new Thread() {
            @Override
            public void run() {
                while(true) {
                    ArrayList<String> cloudChanges = getS3Filenames(s3client);
                    ArrayList<String> localFiles = getLocalFilenames();

                    try {
                        ArrayList<String> filesToDownload = d.getFilenamesToDownload(cloudChanges, localFiles);
                        if(filesToDownload != null) {
                        	if(d.getRemoveLocal() == 0) {
	                            d.downloadAllFiles(s3client, filesToDownload);
	                            filesToDownload.clear();
	                            downloadedFromCloud = true;
	                            Thread.sleep(500);
                        	} else {
                        		System.out.println("antes de chamar o deleteFiles");
                        		d.deleteFiles(s3client, filesToDownload);
                        		filesToDownload.clear();
                        		downloadedFromCloud = true;
                        		Thread.sleep(500);
                        	}
                        }

                        Thread.sleep(2000);
                    } catch (IOException | InterruptedException ex) {
                        Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
           }
        }).start();
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        ClientConfiguration clientConfig = new ClientConfiguration();
        clientConfig.setProtocol(Protocol.HTTP);
        AmazonS3 s3client = new AmazonS3Client(new ProfileCredentialsProvider(), clientConfig);

        synchronize(s3client); 
        System.out.println("Servidor (S3) e o diretório local foram sincronizados.");
        WatchDir(s3client); 

    }
}
