package SD;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;

public class Download {
	
	private static String bucketName;
	private static String projectPath;
	private int removeLocal = 0;

	public Download(String bucketName, String projectPath){
		this.bucketName = bucketName;
		this.projectPath = projectPath;
	}
	
	 public static void getObject(AmazonS3 s3client, String getFileName) throws IOException {
	        String key = getFileName;

	        try {
	            System.out.println("Baixando o arquivo: " + getFileName);
	            S3Object s3object = s3client.getObject(new GetObjectRequest(
	                    bucketName, key));

	            saveFile(s3object.getObjectContent(), getFileName);

	        } catch (AmazonServiceException ase) {
	            System.out.println("Caught an AmazonServiceException, which" +
	                    " means your request made it " +
	                    "to Amazon S3, but was rejected with an error response" +
	                    " for some reason.");
	            System.out.println("Error Message:    " + ase.getMessage());
	            System.out.println("HTTP Status Code: " + ase.getStatusCode());
	            System.out.println("AWS Error Code:   " + ase.getErrorCode());
	            System.out.println("Error Type:       " + ase.getErrorType());
	            System.out.println("Request ID:       " + ase.getRequestId());
	        } catch (AmazonClientException ace) {
	            System.out.println("Caught an AmazonClientException, which means"+
	                    " the client encountered " +
	                    "an internal error while trying to " +
	                    "communicate with S3, " +
	                    "such as not being able to access the network.");
	           System.out.println("Error Message: " + ace.getMessage());
	        }
	    }
	 
	 public static void saveFile(S3ObjectInputStream input, String nameFile)
     throws IOException {

        OutputStream outputStream = null;
        outputStream = new FileOutputStream(new File(projectPath + "Arquivos/" + nameFile));

        int read = 0;
        byte[] bytes = new byte[10250000];

        while ((read = input.read(bytes)) != -1) {
            outputStream.write(bytes, 0, read);
        }

        System.out.println("O arquivo '" + nameFile + "' foi salvado no diretório local.");
        outputStream.close();
     }
	 
	 public ArrayList<String> getFilenamesToDownload(ArrayList<String> S3Files, ArrayList<String> localFiles) {
	        boolean isNot;
	        boolean got = false;
	        removeLocal =  0;
	        ArrayList<String> toDownload = new ArrayList<String>();
	        ArrayList<String> toDelete = new ArrayList<String>();

	        if(S3Files.size() >= localFiles.size()) {
	        // procura pelos elementos que existem no servidor mas nao no local
		        for(int i = 0; i < S3Files.size(); i++) {
		            isNot = true;
		            for(int j = 0; j < localFiles.size(); j++) {
		                if(S3Files.get(i).equals(localFiles.get(j))) {
		                    isNot = false;
		                    got = true;
		                }
		            }
	
		            if(isNot)
		                toDownload.add(S3Files.get(i));
		        }
	
		        if(got) {
		            Collections.sort(toDownload);
		            //removeLocal = 0;
		            return toDownload;
		        }
		        else
		            return null;
	        } else {
	        	
	        	 for(int i = 0; i < localFiles.size(); i++) {
			            isNot = true;
			            for(int j = 0; j < S3Files.size(); j++) {
			                if(S3Files.get(j).equals(localFiles.get(i))) {
			                    isNot = false;
			                    got = true;
			                }
			            }
		
			            if(isNot) {
			                toDelete.add(localFiles.get(i));
			            }
			     }
	        	 
			     if(got) {
			    	 Collections.sort(toDelete);
			    	 removeLocal = 1;
			         return toDelete;
			     }
			     else
			    	 return null;
	        	
	        	
	        	
	        	//File = S3Files.get(i);
	        	//FileSystems.deleteObject(new DeleteObjectRequest(bucketName, fileName.getFileName().toString()));
	        }
	        
	    }
	 
	 public void downloadAllFiles(AmazonS3 s3client, ArrayList<String> toDownload) throws IOException {
		 for(int i = 0; i < toDownload.size(); i++) {
			 getObject(s3client, toDownload.get(i));
	     }
	 }
	 
	 public void deleteFiles(AmazonS3 s3client, ArrayList<String> toDelete) throws IOException {
		 for(int i = 0; i < toDelete.size(); i++) {
			 File f = new File(projectPath + "Arquivos/" + toDelete.get(i));
			 //getObject(s3client, toDelete.get(i));
			 System.out.println("antes de deletar");
			 f.delete();
		 }
	 }

	public int getRemoveLocal() {
		return removeLocal;
	}

	public void setRemoveLocal(int removeLocal) {
		this.removeLocal = removeLocal;
	}
}
