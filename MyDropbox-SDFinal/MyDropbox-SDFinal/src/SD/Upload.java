package SD;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.PutObjectRequest;

public class Upload {
	
	private static String bucketName;
	private static String projectPath;

	public Upload(String bucketName, String projectPath){
		this.bucketName = bucketName;
		this.projectPath = projectPath;
	}
	
	public static void uploadObjectSingleOperation(AmazonS3 s3client, String uploadFileName) {
        String keyName = uploadFileName;//Para facilitar

        try {
            System.out.println("Uploading a new object to S3 from a file\n");
            File file = new File(projectPath + "Arquivos/" + uploadFileName);
            s3client.putObject(new PutObjectRequest(bucketName, keyName, file));

         } catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which " +
                    "means your request made it " +
                    "to Amazon S3, but was rejected with an error response" +
                    " for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which " +
                    "means the client encountered " +
                    "an internal error while trying to " +
                    "communicate with S3, " +
                    "such as not being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }

        System.out.println("O arquivo '" + uploadFileName + "' foi colocado no servidor S3.");
    }
	
	public ArrayList<String> getFilenamesToUpload(ArrayList<String> S3Files, ArrayList<String> localFiles) {
        boolean isNot;
        ArrayList<String> subir = new ArrayList<String>();

        // procura pelos elementos no local que nao existem no servidor
        for(int i = 0; i < localFiles.size(); i++) {
            isNot = true;
            for(int j = 0; j < S3Files.size(); j++) {
                if(localFiles.get(i).equals(S3Files.get(j)))
                    isNot = false;
            }

            if(isNot)
            	subir.add(localFiles.get(i));
        }

        Collections.sort(subir);
        return subir;
    }
	
	public void uploadAllFiles(AmazonS3 s3client, ArrayList<String> toUpload) {
	        for(int i = 0; i < toUpload.size(); i++) {
	            uploadObjectSingleOperation(s3client, toUpload.get(i));
	        }
	 }
}
