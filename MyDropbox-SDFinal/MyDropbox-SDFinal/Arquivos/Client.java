import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.*;

import java.io.File;
import java.io.PrintWriter;

import java.io.FileWriter;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.io.BufferedReader;

import java.io.FileInputStream;
import java.io.BufferedInputStream;


public class Client {
	private Client() {

	}

	static int put_file(UploadFile stub, String name) {
	    //char data[1024];
	   // int read_bytes;
	    //int result;
	    //chunksend chunk;
	    File file;
	    String teste = "";

	   /* clnt = clnt_create(host, FTPPROG, FTPVER, "tcp");
	    if (clnt == NULL) {
	        // nao conectou-se com o servidor
	        clnt_pcreateerror(host);
	        exit(1);
	    }*/

	    file = new File(name);

	    if(!file.exists()) {
	        System.out.println("cliente: nao existe arquivo com esse nome\n");
	        return 1;
	    }

	    //chunk.name = name;

    	try {
    		/*FileReader fileReader = new FileReader(file);
    		BufferedReader bR = new BufferedReader(fileReader);

    		String linha = "";*/

    		FileInputStream f = new FileInputStream(file);
    		BufferedInputStream bf = new BufferedInputStream(f);
    		byte []mydata = new byte[1024*1024];
    		int teste1 = bf.read(mydata);
    		while(teste1 > 0) {
    			teste = stub.uploadFile(name, mydata);
    			teste1 = bf.read(mydata);
    		}
    		/*while((teste1 = bf.read()) != -1) {
    			//System.out.println(Integer.toString(teste1));
    			teste = stub.uploadFile(Integer.toString(teste1));
    		}*/


    		/*File f1=new File(file);
			FileInputStream in=new FileInputStream(f1);
			byte [] mydata=new byte[1024*1024];
			int mylen=in.read(mydata);
			while(mylen>0){
				c.sendData(f1.getName(), mydata, mylen);
				mylen=in.read(mydata);
			}*/



    		/*System.out.println("antes de ler do arquivo:\n ");
    		while((linha = bR.readLine()) != null) {
    			System.out.println(linha);
    			// enviando a linha para o servidor poder escrever
    			teste = stub.uploadFile(linha);
    		}
    		System.out.println("\n\ndepois de ler do arquivo:\n ");
*/

    		/*fileReader.close();
    		bR.close();*/
    	} catch (IOException e) {
	    	e.printStackTrace();
	    }

		        /*read_bytes = fread(data, 1, 1024, file);

		        memcpy(chunk.data, data, read_bytes);
		        chunk.bytes = read_bytes;
		        result = send_file_1(&chunk, clnt);

		        if (result == NULL) {
		            // erro ao chamar o servidor
		            clnt_perror(clnt, host);
		            exit(1);
		        }

		        if (*result != 0) {
		            //erro de sistema remoto
		            errno = *result;
		            perror(name);
		            exit(1);
		        }

		        if (read_bytes < MAXLEN)
		            break;
		    }
	    }

	    fclose(file);

	    return 0;*/
	    return 0;
	}

/*	int read_command(char *host) {
	    char command[MAXLEN], filepath[MAXLEN];

	    printf("Nome do arquivo com extensao: ");
	    scanf("%s", filepath);

	    return put_file(host, filepath);

	}*/

	public static void main(String[] args) {

		//System.setProperty("java.security.policy","file:/home/elisa/Desktop/UFSCar/6 semestre/SD/atividade 5 - RMI/Cliente/client.java.policy");

		/*if(System.getSecurityManager() == null) {
			System.setSecurityManager(new RMISecurityManager());
		}*/
		int teste;
		String host = (args.length < 1) ? null : args[0];
		//System.setProperty("java.rmi.server.hostname","192.168.1.101");
		//System.out.println("host: " + host);
		String name = args[1];
		try {
			Registry registry = LocateRegistry.getRegistry(host);
			UploadFile stub = (UploadFile) registry.lookup("UploadFile");
			//String response = stub.uploadFile();
			teste = put_file(stub, name);
			//System.out.println("response: " + response);
			//teste = put_file();
		} catch (Exception e) {
			System.err.println("Client exception: " + e.toString());
			e.printStackTrace();
		}
	}
}
