/*
    Elisa Castro    RA: 587303
    Leticia Berto   RA: 587354
*/

#include <rpc/rpc.h>
#include <stdio.h>
#include "ftp.h"

extern __thread int errno;

int* send_file_1_svc(chunksend *rec, struct svc_req *rqstp) {

    FILE *file;
    static int result;

    file = fopen(rec->name, "a");

    if (file == NULL) {
        result = errno;
        return &result;
    }

    fwrite(rec->data, 1, rec->bytes, file);
    fclose(file);

    result = 0;
    return &result;
}
