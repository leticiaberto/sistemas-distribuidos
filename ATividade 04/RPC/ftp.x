const MAXLEN = 1024;

/*
 * Type for storing path
 */
typedef string filename<MAXLEN>;


/*
 * Type for storing a chunk of the file that is being
 * sent from the server to the client in the current
 * remote procedure call
 */
typedef opaque filechunk[MAXLEN];


/*
 * File data sent by the server from client to store
 * it on the server along with the filename and the
 * number of bytes in the data
 */
struct chunksend {
    filename name;
    filechunk data;
    int bytes;
};

/*
 * Type that represents the structure for file's chunks
 * to be sent to the server
 */
typedef struct chunksend chunksend;

/*
 * Remote procedure defined in the Interface Definition Language
 * of SUN RPC, contains PROGRAM and VERSION name definitions and
 * the remote procedure signature
 */
program FTPPROG {
    version FTPVER {
        int send_file(chunksend *) = 1;
    } = 1;
} = 0x20030011;
