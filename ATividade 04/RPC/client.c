/*
	Elisa Castro	RA:587303
	Leticia Berto	RA: 587354
*/

#include <rpc/rpc.h>
#include <stdio.h>
#include <string.h>
#include "ftp.h"

extern __thread int errno;

int put_file(char *host, char *name)
{
    CLIENT *clnt;
    char data[1024];
    int read_bytes;
    int *result;
    chunksend chunk;
    FILE *file;

    clnt = clnt_create(host, FTPPROG, FTPVER, "tcp");
    if (clnt == NULL) {
        // nao conectou-se com o servidor
        clnt_pcreateerror(host);
        exit(1);
    }

    file = fopen(name, "r");

    if(file == NULL){
        perror("Falha ao tentar abrir o arquivo");
        exit(-1);
    }

    chunk.name = name;

    while (1) {
        read_bytes = fread(data, 1, 1024, file);

        memcpy(chunk.data, data, read_bytes);
        chunk.bytes = read_bytes;
        result = send_file_1(&chunk, clnt);

        if (result == NULL) {
            // erro ao chamar o servidor
            clnt_perror(clnt, host);
            exit(1);
        }

        if (*result != 0) {
            //erro de sistema remoto
            errno = *result;
            perror(name);
            exit(1);
        }

        if (read_bytes < MAXLEN)
            break;
    }

    fclose(file);

    return 0;
}

int read_command(char *host) {
    char command[MAXLEN], filepath[MAXLEN];

    printf("Nome do arquivo com extensao: ");
    scanf("%s", filepath);

    return put_file(host, filepath);

}

int main(int argc, char *argv[]) {

   int result;

   if (argc != 2) {
        fprintf(stderr, "Modo uso: %s <host>\n", argv[0]);
        exit(1);
   }

    result = read_command(argv[1]);

   return 0;
}
