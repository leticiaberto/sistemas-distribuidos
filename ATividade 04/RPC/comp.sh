#!/bin/bash

rm -f client.o ftp_clnt.c ftp_clnt.o ftp.h ftp_svc.c ftp_svc.o ftp_xdr.c ftp_xdr.o server.o

rpcgen -C ftp.x

cc -c client.c -o client.o
cc -c ftp_clnt.c -o ftp_clnt.o
cc -c ftp_xdr.c -o ftp_xdr.o
cc -o client client.o ftp_clnt.o ftp_xdr.o -lnsl

cc -c server.c -o server.o
cc -c ftp_svc.c -o ftp_svc.o
cc -o server server.o ftp_svc.o ftp_xdr.o -lnsl

if [ ! -d server_dir ]; then
 echo "Criando pasta server_dir onde ficará o servidor"
  mkdir server_dir
fi

if [ ! -d client_dir ]; then
  echo "Criando pasta client_dir onde ficará o servidor"
  mkdir client_dir
fi

mv server server_dir
mv client client_dir

cd client_dir


if [ ! -f 1MB.img ]
then
   echo "Criando arquivos de testes em client_dir"
	fallocate -l 1M 1MB.img
	fallocate -l 5M 5MB.img
	fallocate -l 100M 100MB.img
	fallocate -l 1G 1G.img
fi
