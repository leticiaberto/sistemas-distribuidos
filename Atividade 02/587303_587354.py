 # -*- coding: utf-8 -*-
"""
Elisa Castro    587303
Letícia Berto   587354
"""
import socket
import sys
from threading import Thread
from datetime import datetime
import struct
import time

class Fila(object):
    def __init__(self):
        self.dados = []

    def insere(self, elemento):
        self.dados.append(elemento)

    def retira(self):
        return self.dados.pop(0)

    def ordTotal(self, elemento):
        for list in self.dados:
            list[2] = str(list[2]) + str(elemento)

    def imprime(self):
        for list in self.dados:
            print "\tRecurso: " + str(list[0]) + " pId: " + str(list[1]) + " clock: " + str(list[2])

    def vazia(self):
        return len(self.dados) == 0

    def retEnd(self):
        for list in self.dados:
            return list[3]

def enviar():
    global recurso
    global flag
    flag = ""
    recurso = "" #recurso que o processo atual quer acessar - para tratar os tres estados possiveis
    #recurso = raw_input("Digite o recurso que quer acessar: ")
    # Cria o socket datagrama - UDP
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)


    # Seta o TTL das mensagens para 1, para não sair da rede local
    ttl = struct.pack('b', 1)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)

    while True:
        recurso = raw_input("Digite o recurso que quer acessar: ")
        #monta mensagem com recurso compartilhado, seu número de processo e hora corrente(lógica)
        enviar_recurso = recurso + "@#*" + str(pId) + "@#*" + str(clock)

        try:
            sock.sendto(enviar_recurso.encode('utf-8'), multicast_group_cliente) # Envia uma mensagem através do socket para aquela porta (udp)
            numAck = 0;
            while True:
                dados, server = sock.recvfrom(tam_buffer) #recebe resposta do servidor, dados = recurso; server = endereço
                if len(str(dados)) >= 0:
                    # Verificando se o ACK recebido corresponde a mensagem que enviei
                    if dados == "OK":
                        numAck = numAck + 1
                        if numAck == 2: #(n-1) pois no server nao conta o dele como ok. Quantidade de processos definido em um ambiente controlado
                            print "\nCliente: "
                            print ("O acesso ao recurso '" + recurso + "' é meu agora\n")
                            flag = recurso
                            fim = raw_input("Digite 'q' para parar de usar o recurso: ")
                            if fim == "q":
                                print "Liberei o recurso " + flag
                                recurso = ""
                                flag = recurso
                                while not fila.vazia():#manda ok para qm tava na sua fila
                                    end = fila.retEnd()
                                    enviar_recurso = "OK"
                                    sock.sendto(enviar_recurso.encode('utf-8'), end)
                                    fila.retira()
                                break
                else:
                    print("Nada foi recebido!")
        except ValueError:
            print("Erro de conexao no cliente")

def receber():
    global clock
    # Cria o socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    #COLOCA aqui para poder reusar a porta em mais de um servidor - tira o uso exclusivo de apenas um server
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    # ligar com o endereço do servidor
    sock.bind(server_address)

    # Adicionar o socket no grupo multicast de todas as interfaces
    group = socket.inet_aton(multicast_group)
    mreq = struct.pack('4sL', group, socket.INADDR_ANY)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

    # inicializando a fila
    global fila
    fila = Fila()

    while True:
        try:
            dados, server = sock.recvfrom(tam_buffer) # Recebe uma mensagem do tamanho BUFFER_SIZE
            mensagem_servidor = ''
            if len(str(dados)) >= 0:
                print "\nServidor: "
                men_local, pid_local, clock_local = dados.split('@#*')


                #verifica a qual dos 3 estados permanece
                #nao esta acessando o recurso e nao quer acessa-lo
                if recurso != men_local:
                    print "Não estou acessando o recurso e não quero acessar\n"
                    mensagem_servidor = "OK"
                #já tem acesso ao recurso
                elif men_local == flag:
                    fila.insere([men_local, pid_local, clock_local, server])
                    print "já tenho acesso ao recurso"
                #também quer acesso ao recurso, mas ainda não o fez
                elif str(clock_local) < str(clock):
                    mensagem_servidor = "OK"
                elif str(clock) < str(clock_local):#arrumar essa parte
                    fila.insere([men_local, pid_local, clock_local,server])
                elif str(clock) == str(clock_local):
                    if pId < pid_local:
                        fila.insere([men_local, pid_local, clock_local,server])
                #fim verificação de estados
                #clock do processo, atualizado (lamport)
                clock = max(int(clock_local), int(clock)) + 1 #ordenação parcial

                sock.sendto(mensagem_servidor.encode('utf-8'), server) #envia resposta para o cliente
            else:
                print("Nada foi recebido!")
        except ValueError:
            print("Erro de conexao no servidor.")

if __name__== "__main__":
    """
    servidor = "225.123.203.102"
    porta = 8675
    host = "225.123.203.102"
    """
    tam_buffer = 1024

    multicast_group_cliente = ('224.3.29.71', 10000)
    server_address = ('', 10000)
    multicast_group = '224.3.29.71'

    #pega a hora do sistema para definir o clock
    now = datetime.now()
    clock = now.second
    pId = raw_input("Digite o pId do processo: ")
    print ("\nProcesso pId: " + str(pId) + ", Clock: " + str(clock))
    try:
        t = Thread(target=receber, args=())
        f = Thread(target=enviar, args=())
        t.start()
        f.start()
    except:
        print("Falha ao abrir as threads.")
