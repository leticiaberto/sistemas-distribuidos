// Elisa Castro		RA: 587303
// Leticia Berto	RA: 587354

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

public class P3 extends Thread {
    private static int nProcessos = 5;
    private static final String PID = "3";
    private static final String OK = "ok";
    private static final String DERRUBOU = "derrubou";
    private static final String ELEICAO = "eleicao";
    private static final String VOLTOU = "voltou";
    private static final String parametroDefault = "default";
    private static final String COORD = "coordenador";
    private static ServerSocket server;
    private Socket p3Cliente;
    private static ReentrantLock lock = new ReentrantLock();
    private boolean lider = false;

    public P3( Socket p3Cliente ) {

        this.p3Cliente = p3Cliente;
    }

    public static void server() throws IOException {
        server = new ServerSocket(8003);
    }

    public static void inicializaServer() {
        lock.lock();
        new Thread() {
            public void run() {
                try{
                    while(true){
                        Socket listener = server.accept();
                        P3 processo3 = new P3(listener);
                        processo3.start();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();
        lock.unlock();
    }

    public static void iniciaCliente( String tipoMensagem, String destino){
        lock.lock();
        new Thread(){
            public void run(){

                try {
                    if (tipoMensagem.equals(ELEICAO)){
                        if(8000 + nProcessos >= 8004){
                            Socket processo4 = new Socket("localhost", 8004);
                            BufferedWriter escreveProcesso4 = new BufferedWriter(new OutputStreamWriter(processo4.getOutputStream()));
                            escreveProcesso4.write(ELEICAO + "?" + PID);
                            escreveProcesso4.newLine();
                            escreveProcesso4.flush();
                            processo4.close();
                        }
                        if(8000 + nProcessos >= 8005){
                            Socket processo5 = new Socket("localhost", 8005);
                            BufferedWriter escreveProcesso5 = new BufferedWriter(new OutputStreamWriter(processo5.getOutputStream()));
                            escreveProcesso5.write(ELEICAO + "?" + PID);
                            escreveProcesso5.newLine();
                            escreveProcesso5.flush();
                            processo5.close();
                        }
                    }else{
                        if(tipoMensagem.equals(COORD)){
                            Socket processo1 = new Socket("localhost", 8001);
                            BufferedWriter escreveProcesso1 = new BufferedWriter(new OutputStreamWriter(processo1.getOutputStream()));
                            Socket processo2 = new Socket("localhost", 8002);
                            BufferedWriter escreveProcesso2 = new BufferedWriter(new OutputStreamWriter(processo2.getOutputStream()));

                            escreveProcesso1.write(COORD + "?" + PID);
                            escreveProcesso1.newLine();
                            escreveProcesso1.flush();

                            escreveProcesso2.write(COORD + "?" + PID);
                            escreveProcesso2.newLine();
                            escreveProcesso2.flush();

                            processo1.close();
                            processo2.close();
                        }else{
                            if(tipoMensagem.equals(OK)){
                                String porta = "800" + destino;
                                Socket processo = new Socket("localhost", Integer.parseInt(porta));
                                BufferedWriter escreveProcesso = new BufferedWriter(new OutputStreamWriter(processo.getOutputStream()));
                                escreveProcesso.write(OK + "?" + PID);
                                escreveProcesso.newLine();
                                escreveProcesso.flush();
                                processo.close();
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();
        lock.unlock();
    }

    public void run() {
        try{
            BufferedReader entradaProcesso3 = new BufferedReader(new InputStreamReader(p3Cliente.getInputStream()));
            String entrada = entradaProcesso3.readLine();

            do{
                StringTokenizer token = new StringTokenizer(entrada, "?");

                String tipoMensagem = token.nextToken();
                String origem = token.nextToken();

                if(tipoMensagem.equals(OK)) {
                    System.out.println("Processo " + origem + " é maior que eu. Não faço nada");
                } else {
                    if(tipoMensagem.equals(ELEICAO)){

                        System.out.println("Processo " + origem + " quer ser coordenador");

                        if(Integer.parseInt(PID) > Integer.parseInt(origem)){
                            System.out.println("Processo " + origem + " não faz nada. Eu sou maior.");
                            iniciaCliente(OK, origem);

                            if(nProcessos >= Integer.parseInt(PID)){
                                int n = Integer.parseInt(PID) + 1;
                                System.out.println("\tEleicao de " + PID + " para " + n);
                                iniciaCliente(ELEICAO, parametroDefault);
                            }
                            if(nProcessos == Integer.parseInt(PID)){
                                System.out.println("Eu sou o coordenador agora.");
                                iniciaCliente(COORD, parametroDefault);
                            }
                        }
                    }else{
                        if(tipoMensagem.equals(COORD)){
                            System.out.println("O processo " + origem + " é o coordenador");
                        }else{
                            if(tipoMensagem.equals(DERRUBOU)){
                                System.out.println("\n\nCoordenador derrubou\n\n");
                                nProcessos--;
                                iniciaCliente(ELEICAO, parametroDefault);
                            }
                        }
                    }
                }
                entrada = entradaProcesso3.readLine();
            }while(entrada != null);
        }catch ( IOException e ) {
            e.printStackTrace();
        }
    }

    public static void main (String[] args) throws IOException {
        server();
        inicializaServer();
    }
}
