// Elisa Castro		RA: 587303
// Leticia Berto	RA: 587354

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

public class P5 extends Thread {
    private static int nProcessos = 5;
    private static final String PID = "5";
    private static final String OK = "ok";
    private static final String DERRUBOU = "derrubou";
    private static final String ELEICAO = "eleicao";
    private static final String VOLTOU = "voltou";
    private static final String parametroDefault = "default";
    private static final String COORD = "coordenador";
    private static ServerSocket server;
    private Socket p5Cliente;
    private static ReentrantLock lock = new ReentrantLock();
    private boolean lider = false;
    private static boolean caiu = false;

    public P5( Socket p5Cliente ) {

        this.p5Cliente = p5Cliente;
    }

    public static void server() throws IOException {
        server = new ServerSocket(8005);
    }

    public static void inicializaServer() {
        lock.lock();
        new Thread() {
            public void run() {
                try{
                    while(true){
                        Socket listener = server.accept();
                        P5 processo5 = new P5(listener);
                        processo5.start();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();
        lock.unlock();
    }

    public static void iniciaCliente( String tipoMensagem, String destino){
        lock.lock();
        new Thread(){
            public void run(){

                try {
                    if(tipoMensagem.equals(ELEICAO) || tipoMensagem.equals(COORD)){
                        if(!caiu){
                            Socket processo1 = new Socket("localhost", 8001);
                            BufferedWriter escreveProcesso1 = new BufferedWriter(new OutputStreamWriter(processo1.getOutputStream()));
                            Socket processo2 = new Socket("localhost", 8002);
                            BufferedWriter escreveProcesso2 = new BufferedWriter(new OutputStreamWriter(processo2.getOutputStream()));
                            Socket processo3 = new Socket("localhost", 8003);
                            BufferedWriter escreveProcesso3 = new BufferedWriter(new OutputStreamWriter(processo3.getOutputStream()));
                            Socket processo4 = new Socket("localhost", 8004);
                            BufferedWriter escreveProcesso4 = new BufferedWriter(new OutputStreamWriter(processo4.getOutputStream()));

                            escreveProcesso1.write(COORD + "?" + PID);
                            escreveProcesso1.newLine();
                            escreveProcesso1.flush();

                            escreveProcesso2.write(COORD + "?" + PID);
                            escreveProcesso2.newLine();
                            escreveProcesso2.flush();

                            escreveProcesso3.write(COORD + "?" + PID);
                            escreveProcesso3.newLine();
                            escreveProcesso3.flush();

                            escreveProcesso4.write(COORD + "?" + PID);
                            escreveProcesso4.newLine();
                            escreveProcesso4.flush();

                            processo1.close();
                            processo2.close();
                            processo3.close();
                            processo4.close();

                            caiu = false;
                        }
                    } else {
                        if(tipoMensagem.equals(DERRUBOU)) {
                            Socket processo1 = new Socket("localhost", 8001);
                            BufferedWriter escreveProcesso1 = new BufferedWriter(new OutputStreamWriter(processo1.getOutputStream()));
                            Socket processo2 = new Socket("localhost", 8002);
                            BufferedWriter escreveProcesso2 = new BufferedWriter(new OutputStreamWriter(processo2.getOutputStream()));
                            Socket processo3 = new Socket("localhost", 8003);
                            BufferedWriter escreveProcesso3 = new BufferedWriter(new OutputStreamWriter(processo3.getOutputStream()));
                            Socket processo4 = new Socket("localhost", 8004);
                            BufferedWriter escreveProcesso4 = new BufferedWriter(new OutputStreamWriter(processo4.getOutputStream()));

                            escreveProcesso1.write(DERRUBOU + "?" + PID);
                            escreveProcesso1.newLine();
                            escreveProcesso1.flush();

                            escreveProcesso2.write(DERRUBOU + "?" + PID);
                            escreveProcesso2.newLine();
                            escreveProcesso2.flush();

                            escreveProcesso3.write(DERRUBOU + "?" + PID);
                            escreveProcesso3.newLine();
                            escreveProcesso3.flush();

                            escreveProcesso4.write(DERRUBOU + "?" + PID);
                            escreveProcesso4.newLine();
                            escreveProcesso4.flush();

                            processo1.close();
                            processo2.close();
                            processo3.close();
                            processo4.close();

                            caiu = true;
                        }else{
                            if(tipoMensagem.equals(VOLTOU)){
                                Socket processo1 = new Socket("localhost", 8001);
                                BufferedWriter escreveProcesso1 = new BufferedWriter(new OutputStreamWriter(processo1.getOutputStream()));
                                Socket processo2 = new Socket("localhost", 8002);
                                BufferedWriter escreveProcesso2 = new BufferedWriter(new OutputStreamWriter(processo2.getOutputStream()));
                                Socket processo3 = new Socket("localhost", 8003);
                                BufferedWriter escreveProcesso3 = new BufferedWriter(new OutputStreamWriter(processo3.getOutputStream()));
                                Socket processo4 = new Socket("localhost", 8004);
                                BufferedWriter escreveProcesso4 = new BufferedWriter(new OutputStreamWriter(processo4.getOutputStream()));

                                escreveProcesso1.write(COORD + "?" + PID);
                                escreveProcesso1.newLine();
                                escreveProcesso1.flush();

                                escreveProcesso2.write(COORD + "?" + PID);
                                escreveProcesso2.newLine();
                                escreveProcesso2.flush();

                                escreveProcesso3.write(COORD + "?" + PID);
                                escreveProcesso3.newLine();
                                escreveProcesso3.flush();

                                escreveProcesso4.write(COORD + "?" + PID);
                                escreveProcesso4.newLine();
                                escreveProcesso4.flush();

                                processo1.close();
                                processo2.close();
                                processo3.close();
                                processo4.close();

                            }else{
                                if(tipoMensagem.equals(OK)){
                                    String porta = "800" + destino;
                                    Socket processo = new Socket("localhost", Integer.parseInt(porta));
                                    BufferedWriter escreveProcesso = new BufferedWriter(new OutputStreamWriter(processo.getOutputStream()));
                                    escreveProcesso.write(OK + "?" + PID);
                                    escreveProcesso.newLine();
                                    escreveProcesso.flush();
                                    processo.close();
                                }
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();
        lock.unlock();
    }

    public void run() {
        try{
            BufferedReader entradaProcesso5 = new BufferedReader(new InputStreamReader(p5Cliente.getInputStream()));
            String entrada = entradaProcesso5.readLine();

            do{
                StringTokenizer token = new StringTokenizer(entrada, "?");

                String tipoMensagem = token.nextToken();
                String origem = token.nextToken();

                if(tipoMensagem.equals(OK)){
                    System.out.println("Processo " + origem + " é maior que eu. Não faço nada");
                }else{
                    if(tipoMensagem.equals(ELEICAO)){

                        System.out.println("Processo " + origem + " quer ser coordenador");

                        if(Integer.parseInt(PID) > Integer.parseInt(origem)){
                            System.out.println("Processo " + origem + " não faz nada. Eu sou maior.");
                            iniciaCliente(OK, origem);

                            if(nProcessos >= Integer.parseInt(PID)){
                                int n = Integer.parseInt(PID) + 1;
                                System.out.println("\tEleicao de " + PID + " para " + n);
                                iniciaCliente(ELEICAO, parametroDefault);
                            }
                            if(nProcessos == Integer.parseInt(PID)){
                                System.out.println("\nEu sou o coordenador agora.");
                                iniciaCliente(COORD, parametroDefault);
                            }
                        }
                    }else{
                        if(tipoMensagem.equals(COORD)){
                            System.out.println("O processo " + origem + " é o coordenador");
                        }else{
                            if(tipoMensagem.equals(DERRUBOU)){
                                System.out.println("\n\nCoordenador derrubou\n\n");
                                nProcessos--;
                                iniciaCliente(ELEICAO, parametroDefault);
                            }else{
                                if(tipoMensagem.equals(VOLTOU)){
                                    System.out.println("\nEu sou o coordenador agora.");
                                    iniciaCliente(COORD, parametroDefault);
                                }
                            }
                        }
                    }
                }
                entrada = entradaProcesso5.readLine();
            }while(entrada != null);
        }catch ( IOException e ) {
            e.printStackTrace();
        }
    }

    public static void main (String[] args) throws IOException {
        server();
        inicializaServer();
        Scanner terminalInput = new Scanner(System.in);
        int op;
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("1 - Derrubar o processo");
        op = terminalInput.nextInt();
        iniciaCliente(DERRUBOU, parametroDefault);

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("2 - Colocar o processo de volta");
        op = terminalInput.nextInt();
        iniciaCliente(VOLTOU, parametroDefault);
    }
}
