import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import java.io.File;
import java.io.PrintWriter;

import java.io.FileWriter;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.io.BufferedReader;

import java.io.FileInputStream;
import java.io.BufferedInputStream;


public class Client {
	private Client() {

	}

	static int put_file(UploadFile stub, String name) {
	   
	    File file;
	    String teste = "";

	    file = new File(name);

	    if(!file.exists()) {
	        System.out.println("cliente: nao existe arquivo com esse nome\n");
	        return 1;
	    }

	    
    	try {
    		FileInputStream f = new FileInputStream(file);
    		BufferedInputStream bf = new BufferedInputStream(f);
    		byte []mydata = new byte[1024*1024];
    		int teste1 = bf.read(mydata);
    		while(teste1 > 0) {
    			teste = stub.uploadFile(name, mydata);
    			teste1 = bf.read(mydata);
    		}
    	} catch (IOException e) {
	    	e.printStackTrace();
	    }
	    return 0;
	}

	public static void main(String[] args) {

		int teste;
		String host = (args.length < 1) ? null : args[0];
		String name = args[1];
		try {
			Registry registry = LocateRegistry.getRegistry(host);
			UploadFile stub = (UploadFile) registry.lookup("UploadFile");
			teste = put_file(stub, name);
		} catch (Exception e) {
			System.err.println("Client exception: " + e.toString());
			e.printStackTrace();
		}
	}
}
