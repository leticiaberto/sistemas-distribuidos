import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;


import java.io.File;
import java.io.PrintWriter;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.io.BufferedReader;

import java.io.FileOutputStream;
import java.io.BufferedOutputStream;


public class Server implements UploadFile{
	public Server() {}
	public String uploadFile(String name, byte[] linha) {
		File testeCria = new File(name);
    	try {

    		if(!testeCria.exists())
    			testeCria.createNewFile();

    		FileOutputStream fo = new FileOutputStream(testeCria,true);
			BufferedOutputStream bos = new BufferedOutputStream(fo);
			bos.write(linha);
			bos.flush();
    	} catch (Exception e) {
    		System.out.println("deu ruim no servidor");
    		e.printStackTrace();
    	}

		return "passei no teste de arquivo\n";
	}

	public static void main(String args[]) {

		try {
			Server obj = new Server();
			UploadFile stub = (UploadFile) UnicastRemoteObject.exportObject(obj, 0);
			Registry registry = LocateRegistry.getRegistry();
			registry.bind("UploadFile", stub);
			System.err.println("Server ready");
		} catch (Exception e) {
			System.err.println("Server exception: " + e.toString());
			e.printStackTrace();
		}
	}
}
